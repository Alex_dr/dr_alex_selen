package utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.assertEquals;


public class Checkers {

    public static void verifyText(String expectedText, String actualText) {
        try {
            assertEquals(expectedText, actualText);
            Logger.info(expectedText+ " IS EQUAL "+ actualText);
        } catch (Error e) {
            Logger.info("ERROR "+e.toString());
        }
    }
    public static void verifyIsUniqueItems(ArrayList[] lists) {
        for (ArrayList list : lists) {
            HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
            Integer count;
            for (Object i : list) {
                count = hashMap.get(i);
                hashMap.put((String) i, count == null ? 1 : count + 1);
            }
            for (Object key : hashMap.keySet().toArray()) {
                if (hashMap.get(key) == 1) {
                    hashMap.remove(key);
                }
            }

            try {
                assertEquals(true, hashMap.size() == 0);
                Logger.info("URLs is unique ");
            } catch (Error e) {
                Logger.info("***FAIL: LIST OF NOT UNIQUE URLS***");
                for (Map.Entry<String, Integer> entry : hashMap.entrySet()) {
                    String url = entry.getKey().toString();
                    ;
                    Integer value = entry.getValue();
                    Logger.info("URL: " + url + " repeated " + value + " times");
                }
                Logger.info("***********************************");
            }
        }
    }
}
