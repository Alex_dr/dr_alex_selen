package utils;


public class Variables {

    // find flights widget form
    public static String originCity = "JFK",
                         destinationCity = "SVO",
                         departureDay = "20",
                         departureMonths = "11",
                         departureYear = "2015",
                         returnDay = "21",
                         returnMonth = "11",
                         returnYear = "2015";
    // Passengers page
    public static String firstName = "Alex",
                         lastName = "Burn",
                         gender = "1",
                         monthDateOfBirth = "3",
                         dayDateOfBirth = "15",
                         yearDateOfBirth = "1980",
                         phoneNumber= "12345",
                         email = "mail@mail.ru",
                         emailConfirm = "mail@mail.ru";

    // Check Availability Page
    public static String city = "New York",
                         property = "The Plaza, A Fairmont Managed Hotel";





}
