package utils;

public class Logger {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(Logger.class);

    private static final String SEPARATOR = "***************************************************************************************";
    private static final String TEST_START = "TEST START: %s";
    private static final String TEST_FINISH = "TEST FINISH: %s";
    private static final String BROWSER_INFO = "BROWSER: %s";
    private static final String INFO_LOG = "INFO: %s";
    private static final String EXCEPTION_LOG = "EXCEPTION: %s";
    private static final String BUTTON_IS_ACTIVE = "BUTTON IS ACTIVE : %s";
    private static final String BUTTON_IS_NOT_ACTIVE = "BUTTON IS NOT ACTIVE : %s";

    public static void logTestStart(String testName) {
        LOGGER.info(SEPARATOR);
        LOGGER.info(String.format(TEST_START, testName));
    }

    public static void logTestFinish(String testName) {
        LOGGER.info(String.format(TEST_FINISH, testName));
        LOGGER.info(SEPARATOR);
    }

    public static void info(String info) {
        LOGGER.info(String.format(INFO_LOG, info));
    }

    public static void logException(String exceptionText) {
        LOGGER.info(String.format(EXCEPTION_LOG, exceptionText));
    }

    public static void logButtonIsActive(String elementName) {
        LOGGER.info(String.format(BUTTON_IS_ACTIVE, elementName));
    }

    public static void logButtonIsNotActive(String elementName) {
        LOGGER.info(String.format(BUTTON_IS_NOT_ACTIVE, elementName));
    }

    public static void logBrowserInfo(String info) {
        LOGGER.info(String.format(BROWSER_INFO, info));
    }
}
