package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.DateHelper;
import utils.Logger;

import java.util.List;
import java.util.Set;

public class FairmontPage extends BasePage {

    public FairmontPage(WebDriver driver) {
        super(driver);
    }

    public void closeNoticedWindowIfExist() {
        String parentWindow = driver.getWindowHandle();
        Set<String> windows = driver.getWindowHandles();
        if (windows.size() > 1) {
            for (String window : windows) {
                if (!window.equals(parentWindow)) {
                    driver.switchTo().window(window);
                    Logger.info("switching to noticed window");
                }
            }
            Logger.info("Closing noticed window");
            driver.close();
            driver.switchTo().window(parentWindow);
        }
    }

    public void closeOpinionMattersWindowIfVisible() {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 6);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@id='invL']")));
            Logger.info("opinion matters window IS VISIBLE");
            Logger.info("closing opinion matters window");
            JavascriptExecutor jse = (JavascriptExecutor) driver;
            jse.executeScript("javascript:clWin();");
        } catch (Exception e) {
            Logger.info("opinion matters window NOT VISIBLE");
        }
    }

    public void selectCurrentDayOfMonth() {
        int dayOfMonth = DateHelper.getDayOfMonth();
        List<WebElement> days = driver.findElements(By.tagName("td"));

        for (WebElement cell : days) {
            if (cell.getText().equals(String.valueOf(dayOfMonth))) {
                Logger.info("Clicking on current day of month : " + String.valueOf(dayOfMonth));
                cell.findElement(By.xpath("//a[text()='" + dayOfMonth + "']")).click();
                break;
            }
        }
    }
}
