package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DeltaPage extends BasePage{

    public DeltaPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//section[@id='nav-widget-booking']")
    private WebElement bookingWidget;

    @FindBy(xpath = "//a[@id='book-air-content-trigger']")
    private WebElement bookingWidgetFlight;

    public void bookATrip(){
        bookingWidget.click();
    }

    public void bookATripFlight(){
        bookingWidgetFlight.click();
    }

}
