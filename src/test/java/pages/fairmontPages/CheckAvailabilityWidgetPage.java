package pages.fairmontPages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import pages.FairmontPage;
import utils.Logger;

public class CheckAvailabilityWidgetPage extends FairmontPage {

    @FindBy(css = "select[id$='ddlDestination']")
    private WebElement destinationCitiesList;

    @FindBy(css = "select[id$='ddlProperty']")
    private WebElement propertiesList;

    @FindBy(css = "input[id$='btnCheckAvailability']")
    private WebElement checkAvailabilityBtn;

    @FindBy(css = "input[id$='ArrivalDate']")
    private WebElement arrivalDateField;

    @FindBy(css = "input[id$='DepartureDate']")
    private WebElement departureDateField;

    @FindBy(xpath = "//div[@id='ui-datepicker-div']/div[1]")
    private WebElement leftCornerDatepicker;

    @FindBy(xpath = "//div[@id='ui-datepicker-div']/div[2]")
    private WebElement rightCornerDatepicker;

    public CheckAvailabilityWidgetPage(WebDriver driver) {
        super(driver);
    }

    public String arrivalDate(){
        return arrivalDateField.getAttribute("value");
    }

    public String departureDate(){
        return departureDateField.getAttribute("value");
    }

    public void selectCityDestination(String city){
        Select cities = new Select(destinationCitiesList);
        cities.selectByVisibleText(city);
        Logger.info("selecting destination city " + city);
    }

    public void selectProperty(String property){
        Select properties = new Select(propertiesList);
        properties.selectByVisibleText(property);
        Logger.info("Selecting property " + property);
    }

    public void clickCheckAvailabilityBtn(){
        waitForElementIsClickable(checkAvailabilityBtn);
        checkAvailabilityBtn.click();
    }

    public SelectARoomPage checkAvailability(){
        clickCheckAvailabilityBtn();
        Logger.info("Clicking check 'Check Availability and Rates' ");
        Logger.info("Navigating to the 'Select a Room' page");
        return PageFactory.initElements(driver, SelectARoomPage.class);
    }

    public void openDatePickerArrivalDateField(){
        arrivalDateField.click();
        Logger.info("Opening datepicker in arrival date field");
    }

    public void openDatePickerDepartureDateField(){
        departureDateField.click();
        Logger.info("Opening datepicker in departure date field");
    }

}
