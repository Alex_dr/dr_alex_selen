package pages.fairmontPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.FairmontPage;
import utils.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class SelectARoomPage extends FairmontPage {

    public SelectARoomPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[contains(@id,'rptMainForRoomClass')]//div[contains(@class,'headingBox')]")
    private List<WebElement> roomClasses;

    @FindBy(css = "div[id$='divMain']>h2>div[class='headingBox']")
    private List<WebElement> headings;

    @FindBy(css = "h2[id$='h2RoomClassHeading']")
    private List<WebElement> subHeadings;

    @FindBy(xpath = "//ul[@class='option-list do-sort']//h2")
    private List<WebElement> roomNames;

    @FindBy(css = "a[id$='hlkRoomRateSummary']")
    private List<WebElement> roomRateSummaryLink;

    @FindBy(css = "a[id$='hlkViewRoom']")
    private List<WebElement> viewRoomLink;

    @FindBy(css = "span[id$='ArrivalDateValue']")
    private WebElement arrivalDateValue;

    @FindBy(css = "span[id$='DepartureDateValue']")
    private WebElement departureDateValue;

    @FindBy(xpath = "//div[@id='divLoader']")
    private WebElement loaderIcon;

    @FindBy(xpath = "//a[@id='ctl00_ctl00_Content_Content_rptMainForRoomClass_ctl00_rptRoomClass_ctl00_rptRooms_ctl00_lnkbtnSlectContinue']/span/span")
    private WebElement selectRoomAndContinue;

    public String arrivalDateValue(){
        return arrivalDateValue.getText();
    }

    public String departureDateValue(){
        return departureDateValue.getText();
    }

    public EnhancePage selectFirstRoom(){
        waitForElementIsClickable( headings.get(0));
        headings.get(0).click();
        int countOfVisibleSubheading = getCountOfVisibleSubheading(subHeadings);
        if (countOfVisibleSubheading == 1) {
            if (subHeadings.get(0).isDisplayed()) {
                subHeadings.get(0).click();
            }
        }
        if (subHeadings.get(0).isDisplayed()) {
            subHeadings.get(0).click();
        }
        Logger.info("Clicking on link selectRoomAndContinue");
        selectRoomAndContinue.click();
        Logger.info("navigating to the 'Enhance' page");
        return PageFactory.initElements(driver, EnhancePage.class);
    }

    public ArrayList[] expandAllRooms() throws InterruptedException {
        List<String> listOfUrlsRoomRateSummaryLinks = new ArrayList<>();
        List<String> listOfUrlsViewRoomsLinks = new ArrayList<>();
        Logger.info("Count of rooms is " + roomNames.size());
        for (WebElement heading : headings) {
            Logger.info("CLASS OPENING - " + heading.getText());
            heading.click();
            int countOfVisibleSubheading = getCountOfVisibleSubheading(subHeadings);
            for (WebElement subHeading : subHeadings) {
                if (countOfVisibleSubheading == 1) {
                    if (subHeading.isDisplayed()) {
                        subHeading.click();
                    }
                }
                if (subHeading.isDisplayed()) {
                    Logger.info("-----SUBCLASS OPENING - " + subHeading.getText());
                    subHeading.click();
                    for (WebElement rateLink : roomRateSummaryLink) {
                        String parentWindow = driver.getWindowHandle();
                        if (rateLink.isDisplayed()) {
                            Logger.info("Clicking Rate Room Summary link ");
                            rateLink.click();
                            Set<String> windows = driver.getWindowHandles();
                            for (String window : windows) {
                                if (!window.equals(parentWindow)) {
                                    driver.switchTo().window(window);
                                    Logger.info("-----URL of Window - " + driver.getCurrentUrl());
                                    listOfUrlsRoomRateSummaryLinks.add(driver.getCurrentUrl());
                                }
                            }
                            driver.close();
                            driver.switchTo().window(parentWindow);
                        }
                    }
                    for (WebElement viewLink : viewRoomLink) {
                        String parentWindow = driver.getWindowHandle();
                        if (viewLink.isDisplayed()) {
                            Logger.info("Clicking View Room link ");
                            viewLink.click();
                            Set<String> windows = driver.getWindowHandles();
                            for (String window : windows) {
                                if (!window.equals(parentWindow)) {
                                    driver.switchTo().window(window);
                                    Logger.info("-----URL of Window - " + driver.getCurrentUrl());
                                    listOfUrlsViewRoomsLinks.add(driver.getCurrentUrl());
                                }
                            }
                            driver.close();
                            driver.switchTo().window(parentWindow);
                        }
                    }
                    subHeading.click();
                }
            }
            heading.click();
        }
        Logger.info("Count of Array is - " + listOfUrlsViewRoomsLinks.size());
        Logger.info("Count of Array is - " + listOfUrlsRoomRateSummaryLinks.size());
        return new ArrayList[] {(ArrayList) listOfUrlsViewRoomsLinks, (ArrayList) listOfUrlsRoomRateSummaryLinks};

    }


    private Integer getCountOfVisibleSubheading(List<WebElement> webElements) {
        int count = 0;
        for (WebElement element : webElements) {
            if (element.isDisplayed()) {
                count++;
            }
        }
        return count;
    }

    public void waitUntilLoaderIsPresent() throws InterruptedException {
        if (loaderIcon.isDisplayed()) {
            Logger.info("Waiting until loader of additional results");
            WebDriverWait wait = new WebDriverWait(driver, 15);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("divLoader")));
            Logger.info("Loader of additional results is invisible");
            Thread.sleep(3000);
        }
    }
}
