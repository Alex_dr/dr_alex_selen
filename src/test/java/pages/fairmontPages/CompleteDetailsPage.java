package pages.fairmontPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.FairmontPage;

public class CompleteDetailsPage extends FairmontPage {

    public CompleteDetailsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "span[id$='ArrivalDateValue']")
    private WebElement arrivalDateValue;

    @FindBy(css = "span[id$='DepartureDateValue']")
    private WebElement departureDateValue;

    public String arrivalDateValue() {
        return arrivalDateValue.getText();
    }

    public String departureDateValue() {
        return departureDateValue.getText();
    }

}
