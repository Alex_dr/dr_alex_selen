package pages.fairmontPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.FairmontPage;
import utils.Logger;

import java.util.List;


public class EnhancePage extends FairmontPage {

    public EnhancePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "span[id$='ArrivalDateValue']")
    private WebElement arrivalDateValue;

    @FindBy(css = "span[id$='DepartureDateValue']")
    private WebElement departureDateValue;

    @FindBy(css = "input[id$='btnSkipStepTop']")
    private WebElement skipStepButton;

    @FindBy(css = "input[id$='chkOpt']")
    private List<WebElement> chkOptCheckbox;

    public CompleteDetailsPage ClickOnSkipStepButton(){
        waitForElementIsClickable(skipStepButton);
        skipStepButton.click();
        Logger.info("Clicking on link Skip Step Button");
        Logger.info("Navigating to the 'Complete Details' page");
        return PageFactory.initElements(driver, CompleteDetailsPage.class);
    }

    public String arrivalDateValue(){
        return arrivalDateValue.getText();
    }

    public String departureDateValue(){
        return departureDateValue.getText();
    }

}
