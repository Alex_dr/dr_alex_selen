package pages.deltaPages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import pages.DeltaPage;
import utils.Logger;

public class PassengersPage extends DeltaPage {

    public PassengersPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//input[@id='firstName0']")
    private WebElement firstNameField;

    @FindBy(xpath = "//input[@id='lastName0']")
    private WebElement lastNameField;

    @FindBy(xpath = "//select[@id='gender0']")
    private WebElement genderList;

    @FindBy(xpath = "//select[@id='month0']")
    private WebElement monthList;

    @FindBy(xpath = "//select[@id='day0']")
    private WebElement dayList;

    @FindBy(xpath = "//select[@id='year0']")
    private WebElement yearList;

    @FindBy(xpath = "//input[@id='emgcFirstName_0']")
    private WebElement emgcFirstNameField;

    @FindBy(xpath = "//input[@id='emgcLastName_0']")
    private WebElement emgcLastNameField;

    @FindBy(xpath = "//input[@id='emgcPhoneNumber_0']")
    private WebElement emgcPhoneNumberField;

    @FindBy(xpath = "//input[@id='telephoneNumber0']")
    private WebElement telephoneNumberField;

    @FindBy(xpath = "//input[@id='email']")
    private WebElement emailField;

    @FindBy(xpath = "//input[@id='reEmail']")
    private WebElement reEmailField;

    @FindBy(xpath = "//button[@id='paxReviewPurchaseBtn']")
    private WebElement continueBtn;

    public void typeFirstName(String firstName){
        firstNameField.clear();
        firstNameField.sendKeys(firstName);
        Logger.info("filling first name field with " + firstName);
    }

    public void typeLastName(String lastName){
        lastNameField.clear();
        lastNameField.sendKeys(lastName);
        Logger.info("filling last name field with " + lastName);
    }

    public void typeEmgcFirstName(String emgcFirstName){
        emgcFirstNameField.clear();
        emgcFirstNameField.sendKeys(emgcFirstName);
        Logger.info("filling first name field  in 'Emergency Contact Information' with " + emgcFirstName);
    }

    public void typeTelephoneNumber(String telephoneNumber){
        telephoneNumberField.clear();
        telephoneNumberField.sendKeys(telephoneNumber);
        Logger.info("filling phone number field in 'Contact Information' with " + telephoneNumber);
    }

    public void typeEmgcPhoneNumber(String emgcPhoneNumber){
        emgcPhoneNumberField.clear();
        emgcPhoneNumberField.sendKeys(emgcPhoneNumber);
        Logger.info("filling phone number field in 'Emergency Contact Information' with " + emgcPhoneNumber);
    }

    public void typeEmgcLastName(String emgcLastName){
        emgcLastNameField.clear();
        emgcLastNameField.sendKeys(emgcLastName);
        Logger.info("filling last name field in 'Emergency Contact Information' with " + emgcLastName);
    }

    public void typeEmail(String email){
        emailField.clear();
        emailField.sendKeys(email);
        Logger.info("filling email field in 'Contact Information' with " + email);
    }

    public void typeReEmail(String reEmail){
        reEmailField.clear();
        reEmailField.sendKeys(reEmail);
        Logger.info("filling confirm email field in 'Contact Information' with " + reEmail);
    }

    public void selectGender(String gender){
        Select genders = new Select(genderList);
        genders.selectByIndex(Integer.parseInt(gender) + 1);
        Logger.info("selecting gender " + gender);
    }

    public void selectMonth(String month){
        Select months = new Select(monthList);
        months.selectByIndex(Integer.parseInt(month)+1);
        Logger.info("selecting month of birth " + month);
    }

    public void selectDay(String day){
        Select days = new Select(dayList);
        days.selectByVisibleText(day);
        Logger.info("selecting day of birth " + day);
    }

    public void selectYear(String year){
        Select years = new Select(yearList);
        years.selectByValue(year);
        Logger.info("selecting year of birth " + year);
    }

    public PaymentPage clickContinueBtn(){
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("scroll(0, 300)");
        continueBtn.click();
        Logger.info("clicking 'Continue' button and navigating to the Payment page");
        return PageFactory.initElements(driver, PaymentPage.class);
    }
}
