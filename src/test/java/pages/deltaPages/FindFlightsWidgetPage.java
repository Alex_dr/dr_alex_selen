package pages.deltaPages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.DeltaPage;
import utils.DateHelper;
import utils.Logger;

import java.util.List;

public class FindFlightsWidgetPage extends DeltaPage {

    public FindFlightsWidgetPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//button[@id='roundTripBtn']")
    private WebElement bookingWidgetFlightRoundTripBtn;

    @FindBy(xpath = "//button[@id='exactDaysBtn']")
    private WebElement exactDaysBtn;

    @FindBy(xpath = "//button[@id='cashBtn']")
    private WebElement cashBtn;

    @FindBy(xpath = "//input[@id='destinationCity']")
    private WebElement destinationCity;

    @FindBy(xpath = "//input[@id='originCity']")
    private WebElement originCity;

    @FindBy(xpath = "//input[@id='departureDate']")
    private WebElement departureDate;

    @FindBy(xpath = "//input[@id='returnDate']")
    private WebElement returnDate;

    @FindBy(xpath = "//button[@id='findFlightsSubmit']")
    private WebElement findFlightsSubmit;

    @FindBy(xpath = "//div[@class='cal1']")
    private WebElement leftCornerDatepicker;

    @FindBy(xpath = "//div[@class='cal2']")
    private WebElement rightCornerDatepicker;

    public void openDatePickerReturnDateField(){
        Logger.info("Opening datepicker in return date field");
        returnDate.click();
    }

    public void openDatePickerDepartureDateField(){
        Logger.info("Opening datepicker in departure date field");
        departureDate.click();
    }

    public void selectCurrentDayOfMonthOfArrivalDate(){
        int dayOfMonth = DateHelper.getDayOfMonth();
        List<WebElement> days = driver.findElements(By.tagName("td"));

        for (WebElement cell : days){
            if(cell.getText().equals(String.valueOf(dayOfMonth))){
                Logger.info("Clicking on current day of month : " + String.valueOf(dayOfMonth));
                cell.findElement(By.xpath("//div[@class='cal2']//td[text()='"+dayOfMonth+"']")).click();
                break;
            }
        }
    }

    public void selectCurrentDayOfMonthOfDepartureDate(){
        int dayOfMonth = DateHelper.getDayOfMonth();
        List<WebElement> days = driver.findElements(By.tagName("td"));

        for (WebElement cell : days){
            if(cell.getText().equals(String.valueOf(dayOfMonth))){
                Logger.info("Clicking on current day of month : " + String.valueOf(dayOfMonth));
                cell.findElement(By.xpath("//div[@class='cal1']//td[text()='"+dayOfMonth+"']")).click();
                break;
            }
        }
    }

    public void clickRoundTripBtn(){
        bookingWidgetFlightRoundTripBtn.click();
        Logger.info("Clicking Round Trip button");
    }

    public void originCity(String city){
        originCity.clear();
        originCity.sendKeys(city);
        Logger.info("filling origin City field with " + city);
    }

    public void destinationCity(String city){
        destinationCity.clear();
        destinationCity.sendKeys(city);
        Logger.info("filling destination City field with " + city);
    }

    public void departureDate(String month, String day, String year) throws InterruptedException {
        departureDate.clear();
        departureDate.sendKeys(month);
        departureDate.sendKeys(day);
        departureDate.sendKeys(year);
        Logger.info("filling departure date field with " + month + '/' + day + '/' + year);
    }

    public void returnDate(String month, String day, String year){
        returnDate.clear();
        returnDate.sendKeys(month);
        returnDate.sendKeys(day);
        returnDate.sendKeys(year);
        Logger.info("filling return date field with " + month + '/' + day + '/' + year);
    }

    public void clickExactDaysBtn(){
        exactDaysBtn.click();
        Logger.info("clicking Exact Dates button");
    }

    public void clickCashBtn(){
        cashBtn.click();
        Logger.info("clicking Money button");
    }

    public TicketsSelectionPage clickFindFlightsSubmit(){
        findFlightsSubmit.click();
        Logger.info("clicking Submit button and navigate to tickets selection page");
        return PageFactory.initElements(driver, TicketsSelectionPage.class);
    }

}
