package pages.deltaPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.DeltaPage;
import utils.Logger;

public class PaymentPage extends DeltaPage {

    public PaymentPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//button[@id='continue_button']")
    private WebElement continueBtn;

    public void isActiveButtonCompletePurchase(){
        Logger.info("Verifying whether 'Complete Purchase' button is active");
        try{
            assert(continueBtn.isEnabled());
            Logger.logButtonIsActive(continueBtn.toString());
        } catch (Error e) {
            Logger.logButtonIsNotActive(continueBtn.toString());
        }
    }
}
