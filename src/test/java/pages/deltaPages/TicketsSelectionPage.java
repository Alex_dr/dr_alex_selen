package pages.deltaPages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.DeltaPage;
import utils.Logger;

public class TicketsSelectionPage extends DeltaPage {

    public TicketsSelectionPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//button[@id='0_0_0']")
    private WebElement selectFirstTicketBtn;

    @FindBy(xpath = "//button[@id='tripSummarySubmitBtn']")
    private WebElement continueBtn;

    @FindBy(xpath = "//div[@alt='tripSummaryCol']")
    private WebElement summary;

    public void clickOnSelectFirstTicketOutboundBtn(){
        Logger.info("Clicking 'Select' button on first ticket Outbound in 'Main Cabin'");
        waitForElementPresent(selectFirstTicketBtn);
        selectFirstTicketBtn.click();
    }

    public void clickOnSelectFirstTicketReturnBtn(){
        Logger.info("Clicking 'Select' button on first ticket Return in 'Main Cabin'");
        waitForElementPresent(selectFirstTicketBtn);
        waitForElementIsClickable(selectFirstTicketBtn);
        selectFirstTicketBtn.click();
    }

    public PassengersPage clickContinueBtn(){
        Logger.info("Clicking 'Continue' button on Tip Summary page and navigating to the Passengers page");
        if(continueBtn.isDisplayed()) {
            JavascriptExecutor jse = (JavascriptExecutor) driver;
            jse.executeScript("scroll(0,500)");
        }
        continueBtn.click();
        return PageFactory.initElements(driver, PassengersPage.class);
    }
}
