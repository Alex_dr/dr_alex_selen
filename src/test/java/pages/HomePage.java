package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import utils.Logger;

public class HomePage extends BasePage {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public DeltaPage openDelta(){
        driver.get("https://www.delta.com/");
        Logger.info("Opening Home page www.delta.com");
        return PageFactory.initElements(driver, DeltaPage.class);
    }

    public FairmontPage openFairmont(){
        driver.get("http://www.fairmont.com");
        Logger.info("Opening Home page www.fairmont.com");
        return PageFactory.initElements(driver, FairmontPage.class);
    }


}
