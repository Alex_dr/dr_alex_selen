package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.*;
import utils.Logger;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class BaseTest {

    protected static WebDriver driver;

    @Parameters("browser")
    @BeforeTest(alwaysRun = true)
    public void startBrowser(String browser){
        Logger.logBrowserInfo(browser);
        switch (browser){
            case "firefox":
                driver = new FirefoxDriver();
                break;
            case "chrome":
                System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");
                driver = new ChromeDriver();
                break;
            case "ie":
                System.setProperty("webdriver.ie.driver", "lib/IEDriverServer.exe");
                driver = new InternetExplorerDriver();
                break;
            default:
                driver = new FirefoxDriver();
        }
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @BeforeMethod(alwaysRun = true)
    public void logTestStart(Method method) {
        Logger.logTestStart(method.getName());
    }

    @AfterMethod(alwaysRun = true)
    public void finish(Method method) {
        Logger.logTestFinish(method.getName());
        driver.manage().deleteAllCookies();
    }

    @AfterTest(alwaysRun = true)
    public void closeBrowser() {
        driver.close();
    }


}
