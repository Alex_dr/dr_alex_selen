package tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.FairmontPage;
import pages.HomePage;
import pages.fairmontPages.CheckAvailabilityWidgetPage;
import pages.fairmontPages.CompleteDetailsPage;
import pages.fairmontPages.EnhancePage;
import pages.fairmontPages.SelectARoomPage;
import utils.Checkers;
import utils.Variables;

import java.util.ArrayList;


public class TestFairmont extends BaseTest {

    public HomePage home;
    public CheckAvailabilityWidgetPage checkAvailabilityWidget;

    @BeforeMethod
    public void setup() {
        home = PageFactory.initElements(driver, HomePage.class);
        checkAvailabilityWidget = PageFactory.initElements(driver, CheckAvailabilityWidgetPage.class);
    }

    @Test
    public void testVerifyUniqueLinks() throws InterruptedException {
        FairmontPage fairmontPage = home.openFairmont();
        fairmontPage.closeNoticedWindowIfExist();
        fairmontPage.closeOpinionMattersWindowIfVisible();
        checkAvailabilityWidget.selectProperty(Variables.property);
        checkAvailabilityWidget.openDatePickerArrivalDateField();
        checkAvailabilityWidget.selectCurrentDayOfMonth();
        checkAvailabilityWidget.openDatePickerDepartureDateField();
        checkAvailabilityWidget.selectCurrentDayOfMonth();
        SelectARoomPage selectARoom = checkAvailabilityWidget.checkAvailability();
        selectARoom.waitUntilLoaderIsPresent();
        ArrayList[] listUrls = selectARoom.expandAllRooms();
        Checkers.verifyIsUniqueItems(listUrls);

    }

    @Test
    public void testVerifyOfArrivalAndDepartureDates() throws InterruptedException {
        FairmontPage fairmontPage = home.openFairmont();
        fairmontPage.closeOpinionMattersWindowIfVisible();

        checkAvailabilityWidget.selectCityDestination(Variables.city);
        checkAvailabilityWidget.selectProperty(Variables.property);
        checkAvailabilityWidget.openDatePickerArrivalDateField();
        checkAvailabilityWidget.selectCurrentDayOfMonth();
        checkAvailabilityWidget.openDatePickerDepartureDateField();
        checkAvailabilityWidget.selectCurrentDayOfMonth();

        String arrivalDate = checkAvailabilityWidget.arrivalDate();
        String departureDate = checkAvailabilityWidget.departureDate();

        SelectARoomPage selectARoom = checkAvailabilityWidget.checkAvailability();

        selectARoom.waitUntilLoaderIsPresent();
        Checkers.verifyText(selectARoom.arrivalDateValue(), arrivalDate);
        Checkers.verifyText(selectARoom.departureDateValue(), departureDate);

        EnhancePage enhancePage = selectARoom.selectFirstRoom();
        Checkers.verifyText(enhancePage.arrivalDateValue(), arrivalDate);
        Checkers.verifyText(enhancePage.departureDateValue(), departureDate);

        CompleteDetailsPage completeDetailsPage = enhancePage.ClickOnSkipStepButton();
        Checkers.verifyText(completeDetailsPage.arrivalDateValue(), arrivalDate);
        Checkers.verifyText(completeDetailsPage.departureDateValue(), departureDate);
    }
}
