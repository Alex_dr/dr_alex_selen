package tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.deltaPages.FindFlightsWidgetPage;
import pages.deltaPages.PassengersPage;
import pages.deltaPages.PaymentPage;
import pages.deltaPages.TicketsSelectionPage;
import utils.Variables;

public class TestDelta extends BaseTest {

    public HomePage home;
    public FindFlightsWidgetPage findFlightsWidget;

    @BeforeMethod
    public void setup(){
        home = PageFactory.initElements(driver, HomePage.class);
        findFlightsWidget = PageFactory.initElements(driver, FindFlightsWidgetPage.class);
    }

    @Test
    public void testFlightCombinationOne() throws InterruptedException {
        home.openDelta();
        findFlightsWidget.clickRoundTripBtn();
        findFlightsWidget.originCity(Variables.originCity);
        findFlightsWidget.destinationCity(Variables.destinationCity);

        findFlightsWidget.openDatePickerDepartureDateField();
        findFlightsWidget.selectCurrentDayOfMonthOfDepartureDate();

        findFlightsWidget.openDatePickerReturnDateField();
        findFlightsWidget.selectCurrentDayOfMonthOfArrivalDate();
        findFlightsWidget.clickExactDaysBtn();
        findFlightsWidget.clickCashBtn();
        TicketsSelectionPage ticketsSelectionPage = findFlightsWidget.clickFindFlightsSubmit();

        ticketsSelectionPage.clickOnSelectFirstTicketOutboundBtn();
        ticketsSelectionPage.clickOnSelectFirstTicketReturnBtn();
        PassengersPage passengersPage = ticketsSelectionPage.clickContinueBtn();

        passengersPage.typeFirstName(Variables.firstName);
        passengersPage.typeLastName(Variables.lastName);
        passengersPage.selectGender(Variables.gender);
        passengersPage.selectMonth(Variables.monthDateOfBirth);
        passengersPage.selectDay(Variables.dayDateOfBirth);
        passengersPage.selectYear(Variables.yearDateOfBirth);
        passengersPage.typeEmgcPhoneNumber(Variables.phoneNumber);
        passengersPage.typeEmgcFirstName(Variables.firstName);
        passengersPage.typeEmgcLastName(Variables.lastName);
        passengersPage.typeTelephoneNumber(Variables.phoneNumber);
        passengersPage.typeEmail(Variables.email);
        passengersPage.typeReEmail(Variables.emailConfirm);
        PaymentPage paymentPage = passengersPage.clickContinueBtn();

        paymentPage.isActiveButtonCompletePurchase();
    }

}
